package fr.fam.melodyexporter;

import java.util.NoSuchElementException;
import java.util.StringTokenizer;

import java.util.concurrent.Callable;

import org.apache.log4j.Logger;

import org.apache.commons.codec.binary.Base64;
import org.apache.http.Header;
import org.apache.http.HttpResponse;
import org.apache.http.HttpStatus;
import org.apache.http.client.fluent.Request;
import org.apache.http.message.BasicHeader;
import org.apache.http.util.EntityUtils;

import fr.fam.melodyexporter.config.Source;
import fr.fam.melodyexporter.config.Metric;

/**
* Scrap metrics from a defined source.
*/
public class MelodyScraper implements Callable<Void> {

    /** Logger. */
    private static final Logger LOGGER = Logger.getLogger(MelodyScraper.class);

    /** Http query base. */
    private static final String LAST_VALUE_BASE_URL = "?part=lastValue";

    /** Http query option. */
    private static final String GRAPH_PARAMETER = "&graph=";

    /** Metrics source. */
    private Source source;

    /** Query url. */
    private String lastValueUrl;

    /** Query header. */
    private Request httpRequest;;

    /**
    * Constructor.
    *
    * @param psource source
    */
    public MelodyScraper(final Source psource) {
        source = psource;

        buildLastValueUrl();
        buildHttpRequest();
    }

    /**
    * Call method, for task execution.
    *
    * @return Void void wrapper
    */
    public final Void call() {

        LOGGER.debug("Scraping source : " + source.toString());

        // init result with empty values
        for (Metric m : source.getMetrics()) {
            m.setValue(-1.0);
        }

        // get the metrics
        String downloadLastValueData = downloadLastValueData();

        // parse the metrics if we have some
        if (downloadLastValueData != null) {
            StringTokenizer rawResultTokens = new StringTokenizer(downloadLastValueData, ",");
            for (Metric m : source.getMetrics()) {
                try {
                    String token = rawResultTokens.nextToken();
                    Double value = Double.parseDouble(token);
                    if (!value.isNaN()) {
                        m.setValue(value);
                    }
                } catch (NoSuchElementException e) {
                    LOGGER.warn("Missing metric, using -1.0");
                }
            }
        } else {
            LOGGER.warn("Missing metrics, using -1.0");
        }
        // nothing to return (Void)
        return null;
    }

    /**
    * Query the source for metrics.
    *
    * @return The javamelody http response or null if the request failed
    */
    private String downloadLastValueData() {
        try {
            // request
            HttpResponse response = httpRequest.execute().returnResponse();

            // check http response status
            int responseCode = response.getStatusLine().getStatusCode();
            if (responseCode == HttpStatus.SC_OK) {
                LOGGER.debug("HTTP-Response OK ");
                return EntityUtils.toString(response.getEntity());
            } else {
                LOGGER.warn("HTTP-Response code was " + responseCode + " for " + lastValueUrl);
                return null;
            }
        // we catch all exceptions to keep the app running
        } catch (Exception e) {
            LOGGER.warn("Exception while downloading: " + lastValueUrl, e);
            return null;
        }
    }

    /**
    * Encode credentials in base64.
    *
    * @param login login
    * @param password password
    * @return base64 encoded credentials
    */
    private String buildBasicAuthHeaderValue(final String login, final String password) {
        String credentials = login + ":" + password;
        return Base64.encodeBase64String(credentials.getBytes());
    }

    /**
    * Build the javamelody request url.
    */
    private void buildLastValueUrl() {

        StringBuilder sBuilder = new StringBuilder(source.getUrl());

        sBuilder.append(LAST_VALUE_BASE_URL);
        sBuilder.append(GRAPH_PARAMETER);
        int size = source.getMetrics().size();

        for (Metric m : source.getMetrics()) {
            sBuilder.append(m.getName());
            if (--size > 0) {
                sBuilder.append(",");
            }
        }
        lastValueUrl = sBuilder.toString();
    }

    /**
    * Build the http request.
    */
    private void buildHttpRequest() {
        Header authHeader;

        // build the http request
        Request request = Request.Get(lastValueUrl)
            .connectTimeout(source.getTimeout())
            .socketTimeout(source.getTimeout());

        // create basic authentification header if needed
        if (source.getLogin() != null && source.getPassword() != null) {
            authHeader = new BasicHeader("Authorization", "Basic"
                + buildBasicAuthHeaderValue(source.getLogin(), source.getPassword()));
            request = request.addHeader(authHeader);
        }

        httpRequest = request;
    }

}
