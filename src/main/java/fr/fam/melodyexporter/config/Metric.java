package fr.fam.melodyexporter.config;

import java.util.Map;
import java.util.HashMap;

/**
*
*/
public class Metric {
    /** metric short name. */
    private String name;

    /** */
    private Map<String, String> labels = new HashMap<String, String>();

    /** */
    private Double value;

    /**
    *
    * @return name
    */
    public final String getName() {
        return name;
    }

    /**
    *
    * @param pname application name
    */
    public final void setName(final String pname) {
        name = pname;
    }

    /**
    *
    * @return labels
    */
    public final Map<String, String> getLabels() {
        return labels;
    }

    /**
    *
    * @param plabels application labels
    */
    public final void setLabels(final Map<String, String> plabels) {
        labels = plabels;
    }

    /**
    *
    * @param plabels plabels
    */
    public final void addLabels(final Map<String, String> plabels) {
        labels.putAll(plabels);
    }

    /**
    *
    * @return value
    */
    public final Double getValue() {
        return value;
    }

    /**
    *
    * @param pvalue value
    */
    public final void setValue(final Double pvalue) {
        value = pvalue;
    }

    /**
    *
    * @return string
    */
    @Override
    public final String toString() {
        StringBuilder s = new StringBuilder();

        s.append("{name=" + name + ", ");

        // browsing labels
        s.append("labels=[");

        for (Map.Entry<String, String> e : labels.entrySet()) {
            s.append("{");
            s.append(e.getKey());
            s.append(", ");
            s.append(e.getValue());
            s.append("}, ");
        }
        s.append("]");

        s.append("}");

        return s.toString();
    }
}
