package fr.fam.melodyexporter;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.HashSet;

import java.util.concurrent.Executors;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.TimeUnit;

import org.apache.log4j.Logger;

import fr.fam.melodyexporter.config.MelodyConfig;
import fr.fam.melodyexporter.config.Applications;
import fr.fam.melodyexporter.config.Application;
import fr.fam.melodyexporter.config.Source;
import fr.fam.melodyexporter.config.Metric;
import io.prometheus.client.Collector;
import io.prometheus.client.GaugeMetricFamily;

/**
*
*/
public class MelodyCollector extends Collector {

    /** full scraping timeout (in seconds). */
    static final long COLLECTOR_TIMEOUT = 10L;

    /** */
    private static final Logger LOGGER = Logger.getLogger(MelodyCollector.class);

    /** */
    private Set<MelodyScraper> scrapers = new HashSet<MelodyScraper>();

    /** */
    private MelodyConfig config;

    /** */
    private Applications applications;

    /** threads pool. */
    private ExecutorService scrapPool;

    /**
    *
    * @param pconfig configuration
    */
    public MelodyCollector(final MelodyConfig pconfig) {
        super();
        config = pconfig;
        applications = config.getApplications();

        // populate scrapers for each source
        // for each app
        for (Application application : applications.getApplications()) {

            // for each source
            for (Source source : application.getSources()) {

                // create and add scraper
                scrapers.add(new MelodyScraper(source));
            }

        }

        // create the thread pool
        scrapPool = Executors.newFixedThreadPool(scrapers.size());
    }

    /**
    *
    */
    public final void close() {
        scrapPool.shutdownNow();
    }

    /**
    *
    * @throws IllegalStateException
    */
    @Override
    public final List<MetricFamilySamples> collect() throws IllegalStateException {
        LOGGER.debug("Building samples...");

        List<MetricFamilySamples> mfs = new ArrayList<MetricFamilySamples>();

        // scrap metrics
        // scraping is multithreaded
        // we scrap by sources using sources.getMetrics(), so it's thread safe
        // we use invokeAll to wait for all tasks to finish
        // So, scrapers need to be callable objects
        try {
            scrapPool.invokeAll(scrapers, COLLECTOR_TIMEOUT, TimeUnit.SECONDS);
        } catch (InterruptedException e) {
            LOGGER.warn("Timeout on source scraping thread pool");
        }

        // for each metric
        for (Map.Entry<String, List<Metric>> m : config.getFinalMetrics().entrySet()) {

            LOGGER.debug("New metric : " + m.getKey());

            List<String> labelKeys = new ArrayList<String>();

            // get metrics labels keys (use the first metric)
            for (Map.Entry<String, String> e : m.getValue().get(0).getLabels().entrySet()) {
                labelKeys.add(e.getKey());
            }

            // create gauge
            GaugeMetricFamily gauge = new GaugeMetricFamily(m.getKey(),
                "Help for " + m.getKey(), labelKeys);

            // for each value
            for (Metric v : m.getValue()) {

                LOGGER.debug("New value : " + v.getValue());

                List<String> labelValues = new ArrayList<String>();

                // get metrics labels values
                for (String s : labelKeys) {
                    labelValues.add(v.getLabels().get(s));
                }

                // populate gauge
                gauge.addMetric(labelValues, v.getValue());

            }

            mfs.add(gauge);

        }

        return mfs;
    }

}
