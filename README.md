# MelodyExporter

This is a prometheus exporter (<https://prometheus.io/docs/instrumenting/exporters>) for the JavaEE monitoring tool [JavaMelody](https://github.com/javamelody/javamelody/wiki). It was created to get a deeper insight of the data JavaMelody collects, especially over time without the need to change existing application code to full prometheus monitoring.

## Why Java?

Instead of using Go or Python this exporter uses Java because you probably already own a Java infrastructure when running JavaMelody so you can deploy this exporter right next to your application.

## How it works

This exporter uses the JavaMelody [lastValue external API](https://github.com/javamelody/javamelody/wiki/ExternalAPI#png-and-lastvalue) to pull data and transforms it to Prometheus gauges. Exporting is done via a simple Java servlet.

## How to use it
The build is maven based so a `mvn package` will create the war file in the `target` folder.

Before doing this you have to create your own `melodyexporter.yml` file and put it in your classpath (with Tomcat, use ${catalina.home}/common/classes/)

Afterwards just deploy the war file on an application server or web container of your choice. (tested with Tomcat 7 & Payara 4.1.1, Java EE 6 or above required)

In 'prometheus.yml' job, set the 'metrics_path:' to '/melodyexporter/metrics'

MelodyExporter also provides an exposed javamelody servlet for self monitoring.

The log level can be modified in the log4j.properties file.

All metrics use the application name as metric prefix.

See the file /src/main/ressources/melodyexporter.yml.example for configuration

## Notes

* To avoid logs flooding, stack traces are ignored (only the first line is reported).
In order to get the full stacke traces, remove "%throwable{1}" in the log4j.properties file.

* Java reflexion eats a lot of permgen space. You can fix this using this jvm option : `-Dsun.reflect.inflationThreshold=2147483647`

* CAUTION : Tabulations are forbidden in yaml files.

* See full documentation : <http://thx8411_j2ee.gitlab.io/melodyexporter/>
